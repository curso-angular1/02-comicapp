import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Heroe } from '../../interfaces/heroes.interface';
import { HeroesService } from '../../services/heroes.service';

@Component({
  selector: 'app-info-heroe',
  templateUrl: './info-heroe.component.html',
})
export class InfoHeroeComponent implements OnInit {

  public heroe: any = {};

  constructor
    (
      private _activatedRoute: ActivatedRoute,
      private _heroeService: HeroesService,
      private _router:Router
    ) {
    this._activatedRoute.params.subscribe(params => {
      this.heroe = this._heroeService.getHeroe(params['id'])

      console.log(this.heroe);
    })
  }

  regresar(){
    this._router.navigate(['/heroes']);
  }

  ngOnInit(): void {

  }

}
