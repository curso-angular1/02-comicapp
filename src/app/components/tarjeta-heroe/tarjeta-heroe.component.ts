import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Heroe } from '../../interfaces/heroes.interface';

@Component({
  selector: 'app-tarjeta-heroe',
  templateUrl: './tarjeta-heroe.component.html',
  styles: [
  ]
})
export class TarjetaHeroeComponent implements OnInit {


  @Input() heroe: Heroe;
  @Input() i: number;
  @Output() verHeroeEmit = new EventEmitter<number>();
  constructor() { }

  ngOnInit(): void {
  }

  verHeroe(){
    this.verHeroeEmit.emit(this.heroe.idx);
  }
}
