import { Component, OnInit } from '@angular/core';
import { Heroe } from '../../../interfaces/heroes.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroesService } from '../../../services/heroes.service';

@Component({
  selector: 'app-busqueda',
  templateUrl: './busqueda.component.html',
})
export class BusquedaComponent implements OnInit {

  public heroes: Heroe[] = [];
  public termino: string;
  constructor
    (
      private _activatedRoute: ActivatedRoute,
      private _heroeService: HeroesService,
      private _router:Router
    ) { }

  ngOnInit(): void {
    this._activatedRoute.params.subscribe(params => {
      this.termino = params['termino'];
      this.heroes = this._heroeService.buscarHeroe(params['termino']);
    })
  }
  verHeroe(index:number){
    this._router.navigate(['/heroe',index])
  }

}
