import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { HeroesComponent } from './components/heroes/heroes.component';
import { InfoHeroeComponent } from './components/info-heroe/info-heroe.component';
import { BusquedaComponent } from './components/shared/busqueda/busqueda.component';

const routes: Routes = [
  { path: 'home',                 component: HomeComponent },
  { path: 'about',                component: AboutComponent },
  { path: 'heroes',               component: HeroesComponent },
  { path: 'heroe/:id',            component: InfoHeroeComponent },
  { path: 'busqueda/:termino',    component: BusquedaComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
